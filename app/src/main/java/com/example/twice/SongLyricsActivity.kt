package com.example.twice

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_song_lyrics.*
import android.util.SparseArray

val ALBUM_LYRICS_ID_PAIRS = listOf(
    intArrayOf(R.array.album_the_story_begins, R.array.lyrics_the_story_begins),
    intArrayOf(R.array.album_page_two, R.array.lyrics_page_two),
    intArrayOf(R.array.album_twicecoaster_lane_1, R.array.lyrics_twicecoaster_lane_1),
    intArrayOf(R.array.album_twicecoaster_lane_2, R.array.lyrics_twicecoaster_lane_2),
    intArrayOf(R.array.album_signal, R.array.lyrics_signal),
    intArrayOf(R.array.album_twicetagram, R.array.lyrics_twicetagram),
    intArrayOf(R.array.album_merry_and_happy, R.array.lyrics_merry_and_happy),
    intArrayOf(R.array.album_what_is_love, R.array.lyrics_what_is_love),
    intArrayOf(R.array.album_summer_nights, R.array.lyrics_summer_nights),
    intArrayOf(R.array.album_bdz_repackage, R.array.lyrics_bdz_repackage),
    intArrayOf(R.array.album_yes_or_yes, R.array.lyrics_yes_or_yes),
    intArrayOf(R.array.album_the_year_of_yes, R.array.lyrics_the_year_of_yes)
)

const val LYRICS_NOT_FOUND = "Lyrics Not Found"

class SongLyricsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_lyrics)
        setSupportActionBar(toolbar)

        val currentAlbumId = intent.getIntExtra(EXTRA_ALBUM_ARRAY_ID, 0)
        val songName = intent.getStringExtra(EXTRA_SONG_NAME)

        this.title = songName

        this.findViewById<TextView>(R.id.song_lyrics).apply {
            this.text = getLyrics(currentAlbumId, songName)
        }
    }

    private fun getLyrics(albumArrayId: Int, songName: String): String {
        for (pair in ALBUM_LYRICS_ID_PAIRS) {
            if (albumArrayId == pair[0]) {
                for (i in 0..(resources.getStringArray(pair[0]).size - 1)) {
                    if (songName == resources.getStringArray(pair[0])[i]) {
                        return parseStringArray(pair[1])[i + 1]
                    }
                }
            }
        }
        return LYRICS_NOT_FOUND
    }

    private fun parseStringArray(stringArrayResourceId: Int): SparseArray<String> {
        val stringArray = resources.getStringArray(stringArrayResourceId)
        val outputArray = SparseArray<String>(stringArray.size)
        for (entry in stringArray) {
            val splitResult = entry.split("\\|".toRegex(), 2).toTypedArray()
            outputArray.put(Integer.valueOf(splitResult[0]), splitResult[1])
        }
        return outputArray
    }
}
