package com.example.twice

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_lyrics_song.*

const val EXTRA_ALBUM_ARRAY_ID = "AlbumArrayId"
const val EXTRA_SONG_NAME = "SongName"

class LyricsSongActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lyrics_song)

        val albumId = intent.getIntExtra(EXTRA_ALBUM_STRING_ID, 0)
        val albumName = intent.getStringExtra(EXTRA_ALBUM_STRING_TEXT)
        val layout = this.findViewById<LinearLayout>(R.id.lyrics_song_layout)
        val albumArrayId = getAlbumArrayId(albumId)

        this.toolbar.title = albumName

        val albumSongButtons = songButtons(albumArrayId)

        for (button in albumSongButtons) {
            button.setOnClickListener {
                val intent = Intent(this, SongLyricsActivity::class.java).apply {
                    putExtra(EXTRA_ALBUM_ARRAY_ID, albumArrayId)
                    putExtra(EXTRA_SONG_NAME, button.text)
                }
                startActivity(intent)
            }
            layout.addView(button)
        }
    }

    private fun getAlbumArrayId(albumStringId: Int): Int {
        return when (albumStringId) {
            R.id.button_the_story_begins -> R.array.album_the_story_begins
            R.id.button_page_two -> R.array.album_page_two
            R.id.button_twicecoaster_lane_1 -> R.array.album_twicecoaster_lane_1
            R.id.button_twicecoaster_lane_2 -> R.array.album_twicecoaster_lane_2
            R.id.button_signal -> R.array.album_signal
            R.id.button_twicetagram -> R.array.album_twicetagram
            R.id.button_merry_and_happy -> R.array.album_merry_and_happy
            R.id.button_what_is_love -> R.array.album_what_is_love
            R.id.button_summer_nights -> R.array.album_summer_nights
            R.id.button_bdz_repackage -> R.array.album_bdz_repackage
            R.id.button_yes_or_yes -> R.array.album_yes_or_yes
            R.id.button_the_year_of_yes -> R.array.album_the_year_of_yes
            else -> R.array.album_the_story_begins
        }
    }

    private fun songButtons(albumStringId: Int): List<Button> {
        val songButtons = mutableListOf<Button>()
        for (song in resources.getStringArray(albumStringId)) {
            val newSongButton = songButton(song)
            songButtons.add(newSongButton)
        }
        return songButtons
    }

    private fun songButton(songName: String): Button {
        val btn = Button(this)
        btn.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        btn.text = songName

        return btn
    }
}
