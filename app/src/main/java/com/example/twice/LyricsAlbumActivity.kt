package com.example.twice

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_lyrics_album.*

const val EXTRA_ALBUM_STRING_ID = "AlbumStringId"
const val EXTRA_ALBUM_STRING_TEXT = "AlbumStringText"

class LyricsAlbumActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lyrics_album)
        setSupportActionBar(toolbar)

        val albumButtons = mutableListOf<Button>()

        val layout = this.findViewById(R.id.album_layout) as LinearLayout
        for (i in 0..(layout.childCount - 1)) {
            if (layout.getChildAt(i) is Button) {
                albumButtons.add(layout.getChildAt(i) as Button)
            }
        }

        for (button in albumButtons) {
            button.setOnClickListener {
                val intent = Intent(this, LyricsSongActivity::class.java).apply {
                    putExtra(EXTRA_ALBUM_STRING_ID, button.id)
                    putExtra(EXTRA_ALBUM_STRING_TEXT, button.text)
                }
                startActivity(intent)
            }
        }
    }
}
